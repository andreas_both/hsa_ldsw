package de.hsanhalt.inf.ldsw;

import java.net.URISyntaxException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableAutoConfiguration
public class GameWorld {

	@Bean
	public DataRetriever getDataRetriever( //
			@Value("${sparql.endpoint.domain}") String sparqlEndpointDomain, //
			@Value("${sparql.endpoint.dbname}") String sparqlEndpointDBname, //
			@Value("${sparql.endpoint.user}") String sparqlEndpointUser, //
			@Value("${sparql.endpoint.password}") String sparqlEndpointPassword //
	) throws URISyntaxException {
		Configuration conf = new Configuration(sparqlEndpointDomain, sparqlEndpointDBname, sparqlEndpointUser, sparqlEndpointPassword);
		return new DataRetriever(conf);
	}

	/**
	 * default main, can be removed later
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Properties p = new Properties();
		new SpringApplicationBuilder(GameWorld.class).properties(p).run(args);
		// SpringApplication.run(QanaryService.class, args);
	}
}
