package de.hsanhalt.inf.ldsw;

import java.net.URI;
import java.net.URISyntaxException;


/**
 * collect the external configurations stored in the config file 
 * 
 * @author AnBo
 *
 */
public class Configuration {
	
	private final URI triplestoreEndpointDomain; 
	private final String triplestoreEndpointDBname; 
	private final String triplestoreEndpointUser; 
	private final String triplestoreEndpointPassword; 

	public Configuration(String triplestoreEndpointDomain, String triplestoreEndpointDBname, String triplestoreEndpointUser, String triplestoreEndpointPassword) throws URISyntaxException{
		this.triplestoreEndpointDomain = new URI(triplestoreEndpointDomain);
		this.triplestoreEndpointDBname = triplestoreEndpointDBname;
		this.triplestoreEndpointUser = triplestoreEndpointUser;
		this.triplestoreEndpointPassword = triplestoreEndpointPassword;
	}
		
	public URI getTriplestoreEndpointDomain(){
		return this.triplestoreEndpointDomain;
	}
	
	public String getTriplestoreEndpointDBname(){
		return this.triplestoreEndpointDBname;
	}
	
	public String getTriplestoreEndpointUser(){
		return this.triplestoreEndpointUser;
	}
	
	public String getTriplestoreEndpointPassword(){
		return this.triplestoreEndpointPassword;
	}
	
	
}
