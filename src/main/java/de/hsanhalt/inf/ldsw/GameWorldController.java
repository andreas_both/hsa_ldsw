package de.hsanhalt.inf.ldsw;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.hsanhalt.inf.ldsw.DataRetriever.Language;
import de.hsanhalt.inf.ldsw.data.GameWorldClass;
import de.hsanhalt.inf.ldsw.data.GameWorldConnection;
import de.hsanhalt.inf.ldsw.data.GameWorldInstance;

@Controller
public class GameWorldController {

	private static final Logger logger = LoggerFactory.getLogger(GameWorldController.class);

	private final DataRetriever myDataRetriever;

	@Inject
	public GameWorldController(DataRetriever myDataRetriever) {
		this.myDataRetriever = myDataRetriever;
		logger.info("GameWorldController: {}", this.myDataRetriever);
	}

	/**
	 * provides a HTML page as initial page
	 * 
	 * @return
	 */
	@RequestMapping(value = "/", produces = { "text/html" })
	public String description(Model model) {
		model.addAttribute("dataretriever", myDataRetriever);
		return "index";
	}

	/**
	 * provides a HTML page listing all available GameWorld classes
	 * 
	 * @return
	 */
	@RequestMapping(value = "/class", produces = { "text/html" })
	public String showAllClasses(Model model) {

		model.addAttribute("dataretriever", myDataRetriever);
		model.addAttribute("name", "Classes");
		model.addAttribute("list", myDataRetriever.getGameWorldClasses());

		return "classes";
	}

	/**
	 * provides a HTML page in EN listing all available GameWorld classes
	 * 
	 * @return
	 */
	@RequestMapping(value = "/class/en", produces = { "text/html" })
	public String showAllClassesEn(Model model) {
		DataRetriever.setLanguage(Language.EN);
		return this.showAllClasses(model);
	}
	
	/**
	 * provides a HTML page in DE listing all available GameWorld classes
	 * 
	 * @return
	 */
	@RequestMapping(value = "/class/de", produces = { "text/html" })
	public String showAllClassesDe(Model model) {
		DataRetriever.setLanguage(Language.DE);
		return this.showAllClasses(model);
	}
	
	
	/**
	 * provides a HTML page with one GameWorld class
	 * 
	 * @return
	 */
	@RequestMapping(value = "/class/{encodedUri}", produces = { "text/html" })
	public String showAllClassInstances(@PathVariable String encodedUri, Model model) throws URISyntaxException {
		model.addAttribute("name", "Class");
		model.addAttribute("GameWorldConnection", GameWorldConnection.class);
		model.addAttribute("dataretriever", myDataRetriever);
		model.addAttribute("resource", GameWorldClass.getInstance(encodedUri).getResource());
		model.addAttribute("data", GameWorldClass.getInstance(encodedUri));
		return "classinstances";
	}

	/**
	 * provides a HTML page listing all GameWorld instances
	 * 
	 * @return
	 */
	@RequestMapping(value = "/instance", produces = { "text/html" })
	public String showAllInstances(Model model) {

		model.addAttribute("dataretriever", myDataRetriever);
		model.addAttribute("name", "Instances");
		model.addAttribute("list", myDataRetriever.getGameWorldInstances());

		return "instances";
	}
	
	/**
	 * provides a HTML page in EN listing all GameWorld instances
	 * 
	 * @return
	 */
	@RequestMapping(value = "/instance/en", produces = { "text/html" })
	public String showAllInstancesEn(Model model) {
		DataRetriever.setLanguage(Language.EN);
		return this.showAllInstances(model);
	}
	
	/**
	 * provides a HTML page in DE listing all GameWorld instances
	 * 
	 * @return
	 */
	@RequestMapping(value = "/instance/de", produces = { "text/html" })
	public String showAllInstancesDe(Model model) {
		DataRetriever.setLanguage(Language.DE);
		return this.showAllInstances(model);
	}
	
	

	/**
	 * redirect to demanded entity, provided by POST
	 * 
	 * @return
	 * @throws URISyntaxException 
	 */
	@RequestMapping(value = "/entity/", produces = { "text/html" }, method=RequestMethod.POST)
	public String gotoEntity(@ModelAttribute("entityuri") URI entityUri, HttpServletRequest request) throws URISyntaxException {
		logger.info("redirect to {} -> {}",entityUri,GameWorldConnection.encodedURI(entityUri));
		String redirectUrl = request.getScheme() + "://"+request.getServerName()+":"+request.getServerPort()+GameWorldConnection.encodedURI(entityUri);
		
		DataRetriever.addLastUsedUri(entityUri.toString(), new URI(redirectUrl));
		return "redirect:" + redirectUrl;
	}

	/**
	 * provides a HTML page
	 * 
	 * @return
	 */
	@RequestMapping(value = "/entity/{encodedUri}", produces = { "text/html" })
	public String showInstance(@PathVariable String encodedUri, Model model) throws URISyntaxException {

		model.addAttribute("name", "Entity");
		model.addAttribute("GameWorldConnection", GameWorldConnection.class);
		model.addAttribute("dataretriever", myDataRetriever);
		model.addAttribute("resource", GameWorldInstance.getInstance(encodedUri, this.myDataRetriever).getResource());
		model.addAttribute("data", GameWorldInstance.getInstance(encodedUri, this.myDataRetriever));

		return "entity";
	}

	/**
	 * provides a HTML page
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/entity/{encodedUri}/{language}", produces = { "text/html" })
	public String showInstanceWithLanguage(@PathVariable String encodedUri, @PathVariable String language, Model model)
			throws Exception {
		if (language.toLowerCase().compareTo("en") == 0) {
			DataRetriever.setLanguage(Language.EN);
		} else if (language.toLowerCase().compareTo("de") == 0) {
			DataRetriever.setLanguage(Language.DE);
		} else {
			throw new Exception("Language " + language + " not acceptable, use 'de' or 'en'.");
		}
		return this.showInstance(encodedUri, model);
	}

	/**
	 * provides a JSON response containing the language currently activated
	 * 
	 * @return
	 */
	@RequestMapping(value = "/language/{language}", method = RequestMethod.POST)
	public Boolean switchLanguage(@PathVariable String language) {

		// model.addAttribute("dataretriever", myDataRetriever);

		return true;
	}

}
