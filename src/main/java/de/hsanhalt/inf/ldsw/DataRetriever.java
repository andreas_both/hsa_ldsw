package de.hsanhalt.inf.ldsw;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.Syntax;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.openrdf.query.BindingSet;
import org.openrdf.query.TupleQueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import com.complexible.stardog.api.Connection;
import com.complexible.stardog.api.ConnectionConfiguration;
import com.complexible.stardog.api.SelectQuery;
import com.complexible.stardog.api.reasoning.ReasoningConnection;

import de.hsanhalt.inf.ldsw.data.GameWorldClass;
import de.hsanhalt.inf.ldsw.data.GameWorldConnection;
import de.hsanhalt.inf.ldsw.data.GameWorldInstance;
import de.hsanhalt.inf.ldsw.data.GameWorldLabel;

/**
 * the class is a covering standard tasks users of the Qanary methodology might
 * have
 * 
 * @author AnBo
 *
 */
@EnableAutoConfiguration
public class DataRetriever {

	private static final Logger logger = LoggerFactory.getLogger(DataRetriever.class);

	public final Configuration conf;

	private static DataRetriever myDataRetriever;

	private static String defaultLanguage = "de";

	private static Map<String, List<GameWorldLabel>> cachedLabels = new HashMap<>();
	
	private static ReasoningConnection connectionWithReasoning = null;

	private static Connection connectionWithoutReasoning = null;
	
	private static Map<String, URI> lastUsedUrls = new HashMap<>();
	
	
	public enum Language {EN,DE};
	private static Language language = Language.EN; 

	public DataRetriever(Configuration conf) {
		if (myDataRetriever != null) {
			logger.error("duplicate call");
		} else {
			myDataRetriever = this;
		}

		this.conf = conf;
		
		if( connectionWithoutReasoning == null ){
			connectionWithoutReasoning = ConnectionConfiguration //
					.to(conf.getTriplestoreEndpointDBname()) //
					.server(conf.getTriplestoreEndpointDomain().toString()) //
					.credentials(conf.getTriplestoreEndpointUser(), conf.getTriplestoreEndpointPassword()) //
					.reasoning(false) //
					.connect() //
					.as(Connection.class);			
		}
		if( connectionWithReasoning == null ){
			connectionWithReasoning = ConnectionConfiguration //
					.to(conf.getTriplestoreEndpointDBname()) //
					.server(conf.getTriplestoreEndpointDomain().toString()) //
					.credentials(conf.getTriplestoreEndpointUser(), conf.getTriplestoreEndpointPassword()) //
					.reasoning(true) //
					.connect() //
					.as(ReasoningConnection.class);			
		}
		
		
	}

	/**
	 * return the same instance
	 */
	public static DataRetriever getInstance() {
		return myDataRetriever;
	}

	public static boolean isEnglishLanguageActivated(){
		if( Language.EN == language){
			return true;
		} else {
			return false;
		}
	}
	
	public static void setLanguage(Language language){
		logger.warn("language switched to: {}", language);
		DataRetriever.language = language;
	}
	
	public static Language getLanguage(){
		return DataRetriever.language;
	}
	
	
	public static Map<String,URI> getLastUsedUris(){
		return DataRetriever.lastUsedUrls;
	}
	
	public static void addLastUsedUri( String name, URI uri ){
		DataRetriever.lastUsedUrls.put(name, uri);
	}
	
	
	/**
	 * query a SPARQL endpoint with a given query
	 * 
	 * @param sparqlQuery
	 * @param endpoint
	 * @return
	 */
	public TupleQueryResult selectViaSparqlFromTripleStore(String sparqlQuery, Boolean activateReasoning) {
		long start = getTime();

		// old: Jena code
		Query query = new Query();
		query.setPrefix("owl", "http://www.w3.org/2002/07/owl#");
		query.setPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		query.setPrefix("gw", "http://www.hs-anhalt.de/gameworld/");
		query.setPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");
		query.setPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		query.setPrefix("skos", "http://www.w3.org/2004/02/skos/core#");

		Query query2 = QueryFactory.parse(query, sparqlQuery, null, Syntax.syntaxSPARQL);
		logger.info("selectViaSparqlFromTripleStore:\n {}", query2.toString(Syntax.syntaxSPARQL));

		// new: Sesame code
		Connection aReasoningConn;
		if( activateReasoning){
			aReasoningConn = connectionWithReasoning; 
		} else {
			aReasoningConn = connectionWithoutReasoning; 
		}

		SelectQuery aQuery = aReasoningConn.select(query2.toString());

		TupleQueryResult tupleResult = aQuery.execute();
		logTime(getTime() - start, "selectFromTripleStore: " + sparqlQuery + tupleResult);

		// return resultSet;
		return tupleResult;
	}

	/**
	 * insert data into triplestore
	 * 
	 * @param sparqlQuery
	 * @param endpoint
	 */
	public void updateTripleStore(String sparqlQuery, String endpoint) {
		long start = getTime();
		UpdateRequest request = UpdateFactory.create(sparqlQuery);
		UpdateProcessor proc = UpdateExecutionFactory.createRemote(request, endpoint);
		proc.execute();
		this.logTime(getTime() - start, "updateTripleStore: " + sparqlQuery);
	}

	/**
	 * get current time in milliseconds
	 * 
	 * @return
	 */
	static long getTime() {
		return System.currentTimeMillis();
	}

	/**
	 * 
	 * @param description
	 * @param duration
	 */
	private void logTime(long duration, String description) {
		logger.debug("runtime measurement: {} ms for {}", duration, description);
	}

	/**
	 * get all GameWorldClasses as collection
	 */
	public Collection<GameWorldClass> getGameWorldClasses() {

		List<GameWorldClass> classes = new LinkedList<>();
		// ResultSet resultSet = selectFromTripleStore("SELECT * { ?c
		// rdfs:subClassOf gw:Class }");
		//
		// try {
		// while (resultSet.hasNext()) {
		// classes.add(GameWorldClass.getInstance(new
		// URI(resultSet.next().get("c").asResource().getURI())));
		// }
		// } catch (URISyntaxException e) { // should never happen
		// e.printStackTrace();
		// }

		TupleQueryResult resultSet = selectViaSparqlFromTripleStore(
				"SELECT ?c { ?c rdfs:subClassOf gw:Class } ORDER BY ?c", true);

		String uri;
		try {
			while (resultSet.hasNext()) {
				uri = resultSet.next().getBinding("c").getValue().stringValue();
				logger.warn(uri);
				if (uri.compareTo("http://www.w3.org/2002/07/owl#Nothing") != 0 && uri.compareTo("http://www.hs-anhalt.de/gameworld/Class") != 0) {
					classes.add(GameWorldClass.getInstance(new URI(uri)));
				}
			}
		} catch (URISyntaxException e) { // should never happen
			e.printStackTrace();
		}

		return classes;
	}

	/**
	 * return all properties stored in triplestore for given resource
	 * 
	 * @param resource
	 * @return
	 */
	public List<Entry<URI, Object>> getAllProperties(URI resource) {
		TupleQueryResult resultSet = selectViaSparqlFromTripleStore(
				"SELECT ?p ?o (DATATYPE(?o) AS ?datatype) { <" + resource + "> ?p ?o } ORDER BY ?p", true);

		URI property;
		Object value;
		String valueString;
		BindingSet elem;
		List<Entry<URI, Object>> properties = new LinkedList<>();

		while (resultSet.hasNext()) {
			try {
				elem = resultSet.next();
				// property = new URI(elem.get("p").asResource().getURI());
				property = new URI(elem.getBinding("p").getValue().stringValue());

				valueString = elem.getBinding("o").getValue().stringValue();

				try {
					value = new URI(valueString);
				} catch (Exception e) {
					try {
						value = valueString;
					} catch (Exception e2) {
						logger.warn("{} was neighter a URI nor a Literal: {}.", property, valueString);
						value = "unknown"; // should never happen
					}
				}

				// if (elem.get("o").isResource()) {
				// value = new URI(elem.get("o").asResource().getURI());
				// } else if (elem.get("o").isLiteral()) {
				// value = elem.get("o").asLiteral().getValue();
				// } else {
				// value = "unknown";
				// }
				properties.add(new java.util.AbstractMap.SimpleEntry<>(property, value));
			} catch (URISyntaxException e) {
				logger.error("For URI {} data was not retrievable.", resource);
				e.printStackTrace();
			}
		}

		logger.info("getAllProperties: {} has {} properties", resource, properties.size());
		return properties;
	}

	/**
	 * retrieves all class instances for a given URI
	 * 
	 * @param resource
	 * @return
	 */
	public List<URI> getGameWorldClassInstances(URI resource) {

		List<URI> instances = new LinkedList<>();

		TupleQueryResult resultSet = selectViaSparqlFromTripleStore(
				"SELECT ?instance { ?instance rdf:type <" + resource + "> . }", true);

		try {
			while (resultSet.hasNext()) {
				// instances.add(new
				// URI(resultSet.next().get("instance").asResource().getURI()));
				instances.add(new URI(resultSet.next().getBinding("instance").getValue().stringValue()));
			}
		} catch (URISyntaxException e) { // should never happen
			e.printStackTrace();
		}

		logger.info("for class {} found {} instances", resource, instances.size());
		return instances;
	}

	public String getStandardLabel(String resource) throws URISyntaxException {
		return getAllLabelsAsText(new URI(resource));
	}

	/**
	 * get the label for a resource
	 * 
	 * @param resource
	 * @return
	 */
	public String getStandardLabel(URI resource) {

		List<GameWorldLabel> labels = this.getLabels(resource);

		for (GameWorldLabel gwl : labels) {
			if (gwl.isLanguage(defaultLanguage)) {
				return gwl.toString().concat(" (" + gwl.getLanguage() + ")");
			}
		}

		return resource.toString();
	}

	public String getAllLabelsAsText(String resource) throws URISyntaxException {
		return this.getAllLabelsAsText(new URI(resource));
	}

	/**
	 * returns a tabular separated text containing a text and its language per
	 * line
	 * 
	 * @param resource
	 * @return
	 */
	public String getAllLabelsAsText(URI resource) {
		String text = "";
		for (GameWorldLabel gwl : this.getLabels(resource)) {
			text = text.concat(gwl.toString()).concat("\t(").concat(gwl.getLanguage()).concat(")\n");
		}
		return text;
	}

	/**
	 * if the given resource has a standard label, then return true
	 * 
	 * @param resource
	 * @return
	 * @throws URISyntaxException
	 */
	public Boolean hasStandardLabel(String resource) throws URISyntaxException {
		if (this.getStandardLabel(resource).compareTo(resource) == 0) {
			return false;
		} else {
			return true;
		}
	}

	public Boolean hasStandardLabel(URI resource) throws URISyntaxException {
		return this.hasStandardLabel(resource.toString());
	}

	/**
	 * get all labels for a given resource
	 * 
	 * @param resource
	 * @return
	 */
	public List<GameWorldLabel> getLabels(URI resource) {

		List<GameWorldLabel> myLabels = new LinkedList<>();

		if (logger.isDebugEnabled()) {
			logger.debug("label cache contains {} entries", cachedLabels.size());
			for (String key : cachedLabels.keySet()) {
				logger.debug("label key in cache: {}", key);
			}
		}

		if (cachedLabels.containsKey(resource.toString())) {
			myLabels = cachedLabels.get(resource.toString());
			logger.info("cached {} labels for {}", myLabels.size(), resource);
		} else {
			List<String> labelPredicates = new LinkedList<>();
			labelPredicates.add("skos:prefLabel");
			labelPredicates.add("skos:altLabel");
			labelPredicates.add("skos:hiddenLabel");
			labelPredicates.add("rdfs:label");

			for (String labelPredicate : labelPredicates) {

				TupleQueryResult resultSet = selectViaSparqlFromTripleStore("SELECT ?label (LANG(?label) AS ?lang) { " //
						+ "  <" + resource + "> " + labelPredicate + " ?label . " //
						+ "}", false);

				BindingSet item;
				while (resultSet.hasNext()) {
					item = resultSet.next();

					try {
						myLabels.add(new GameWorldLabel( //
								labelPredicate, //
								item.getBinding("label").getValue().stringValue(), //
								item.getBinding("lang").getValue().stringValue()) //
						);
					} catch (Exception e) {
						logger.error("{} at {}", e.getMessage(), item.getBinding("label").getValue().stringValue());
					}

				}
			}
			cachedLabels.put(resource.toString(), myLabels);
			logger.info("(not cached) for {} retrieved {} labels", resource, myLabels.size());
		}

		return myLabels;
	}

	/**
	 * fetch all available GameWorld instances from triplestore
	 * 
	 * @return
	 */
	public Collection<GameWorldInstance> getGameWorldInstances() {
		List<GameWorldInstance> instances = new LinkedList<>();

		TupleQueryResult resultSet = selectViaSparqlFromTripleStore(
				"SELECT DISTINCT ?instance { ?instance rdf:type ?p. ?p rdfs:subClassOf gw:Class. } ORDER BY ?instance", true);

		try {
			while (resultSet.hasNext()) {
				instances.add(new GameWorldInstance(
						new URI(resultSet.next().getBinding("instance").getValue().stringValue())));
			}
		} catch (URISyntaxException e) { // should never happen
			e.printStackTrace();
		}

		logger.info("found {} instances", instances.size());
		return instances;

	}

	/**
	 * retrieve all connections from triplestore
	 * 
	 * @param resource
	 * @return
	 */
	public List<GameWorldConnection> getConnections(URI resource) {

		TupleQueryResult resultSet = selectViaSparqlFromTripleStore("SELECT DISTINCT ?connection ?target { <" + resource
				+ "> ?connection  ?target. ?connection rdfs:subPropertyOf gw:isConnectedTo.  }", true);

		List<GameWorldConnection> connections = new LinkedList<>();

		BindingSet item;
		List<GameWorldLabel> labels;
		URI target;
		URI connection;

		try {
			while (resultSet.hasNext()) {
				item = resultSet.next();

				connection = new URI(item.getBinding("connection").getValue().stringValue());
				target = new URI(item.getBinding("target").getValue().stringValue());
				labels = this.getLabels(connection);

				connections.add(new GameWorldConnection(connection, labels, target));
			}
		} catch (URISyntaxException e) { // should never happen
			e.printStackTrace();
		}

		logger.info("found {} connection", connections.size());
		return connections;

	}

	/**
	 * get all labels for connections
	 */
	public List<Entry<String, String>> getLabelsOfURI(URI uri) {

		List<Entry<String, String>> labels = new LinkedList<>();
		TupleQueryResult resultSet = selectViaSparqlFromTripleStore(
				"SELECT ?label (LANG(?label) AS ?lang) { <" + uri + "> rdfs:label ?label.  } ", false);

		BindingSet item;
		String label;
		String lang;
		
		while (resultSet.hasNext()) {
			item = resultSet.next();

			label = item.getBinding("label").getValue().stringValue();
			lang = item.getBinding("lang").getValue().stringValue();

			labels.add(new java.util.AbstractMap.SimpleEntry<>(label, lang));
		}

		logger.info("for {} found {} label", uri, labels.size());
		return labels;

	}

	/**
	 * fetch all images of a given resource from the triplestore
	 * 
	 * @param resource
	 * @return
	 */
	public List<URI> getAllImages(URI resource) {
		TupleQueryResult resultSet = selectViaSparqlFromTripleStore("SELECT DISTINCT ?image { <" + resource
				+ "> ?connection  ?image. ?connection rdfs:subPropertyOf gw:hasImage.  }", true);

		List<URI> images = new LinkedList<>();

		BindingSet item;
		try {
			while (resultSet.hasNext()) {
				item = resultSet.next();
				images.add(new URI(item.getBinding("image").getValue().stringValue()));
			}
		} catch (URISyntaxException e) { // should never happen
			e.printStackTrace();
		}

		logger.info("found {} images for resource {}", images.size(), resource);
		return images;
	}

}
