package de.hsanhalt.inf.ldsw.data;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * represents a data object containing all properties of a GameWorld class
 * 
 * @author AnBo
 *
 */
public class GameWorldClass extends GameWorldData {


	private static Map<URI, GameWorldClass> myGameWorldClasses = new HashMap<>();

	
	/**
	 * init with a resource URI
	 * 
	 * @param resource
	 */
	GameWorldClass(URI resource){
		super();
		this.setResource(resource);
	}
	
	/**
	 *  Static 'instance' method 
	 */
	public static GameWorldClass getInstance(URI resource) {
		if( !myGameWorldClasses.containsKey(resource) ){
			GameWorldClass myGameWorldClass = new GameWorldClass(resource);
			myGameWorldClasses.put(resource, myGameWorldClass);
		}
		return myGameWorldClasses.get(resource);
	}

	/**
	 *  Static 'instance' method from Base64 encoded string
	 * @throws URISyntaxException 
	 */
	public static GameWorldClass getInstance(String resource64) throws URISyntaxException {
		
		byte[] decodedBytes = Base64.getDecoder().decode(resource64);
		String decodedString = new String(decodedBytes);
		
		return getInstance(new URI(decodedString));
	}

	/**
	 * get all GameWorldClasses
	 * 
	 * @return
	 */
	public Collection<GameWorldClass> getAll() {
		return GameWorldClass.myGameWorldClasses.values();
	}
	

}
