package de.hsanhalt.inf.ldsw.data;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import de.hsanhalt.inf.ldsw.DataRetriever;

public class GameWorldInstance extends GameWorldData {

	private static Map<URI, GameWorldInstance> myGameWorldInstance = new HashMap<>();
	
	/**
	 * init with a resource URI
	 * 
	 * @param resource
	 */
	public GameWorldInstance(URI resource){
		super();
		this.setResource(resource);
	}
	

	/**
	 *  Static 'instance' method 
	 */
	public static GameWorldInstance getInstance(URI resource) {
		if( !myGameWorldInstance.containsKey(resource) ){
			GameWorldInstance myInstance = new GameWorldInstance(resource);
			myGameWorldInstance.put(resource, myInstance);
		}
		return myGameWorldInstance.get(resource);
	}

	/**
	 *  Static 'instance' method from Base64 encoded string
	 * @throws URISyntaxException 
	 */
	public static GameWorldInstance getInstance(String resource64, DataRetriever myDataRetriever) throws URISyntaxException {
		
		byte[] decodedBytes = Base64.getDecoder().decode(resource64);
		String decodedString = new String(decodedBytes);
		
		return getInstance(new URI(decodedString));
	}

	/**
	 * get all GameWorldClasses
	 * 
	 * @return
	 */
	public Collection<GameWorldInstance> getAll() {
		return GameWorldInstance.myGameWorldInstance.values();
	}
	
	
}
