package de.hsanhalt.inf.ldsw.data;

import java.net.URI;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hsanhalt.inf.ldsw.DataRetriever;
import de.hsanhalt.inf.ldsw.DataRetriever.Language;

public abstract class GameWorldData {

	private static final Logger logger = LoggerFactory.getLogger(GameWorldData.class);

	private List<Entry<URI, Object>> properties = new LinkedList<>();
	private List<GameWorldLabel> labels;
	private URI resource;
	private List<GameWorldConnection> connections;
	private List<GameWorldInstance> instances = new LinkedList<>();

	protected static DataRetriever myDataRetriever;

	public GameWorldData() {
	}

	/**
	 * returns the instance of the data retriever
	 * 
	 * @return
	 */
	public static DataRetriever getDataRetriever() {
		return DataRetriever.getInstance();
	}

	/**
	 * returns resource which is the ID of the information object
	 * 
	 * @return
	 */
	public URI getResource() {
		return this.resource;
	}

	/**
	 * set resource which is the ID of the information object
	 * 
	 * @return
	 */
	public void setResource(URI uri) {
		this.resource = uri;
	}

	/**
	 * returns resource which is the ID of the information object
	 * 
	 * @return
	 */
	public String getResourceEncoded() {
		return "/entity/" + Base64.getEncoder().encodeToString(this.getResource().toString().getBytes()) + "/";
	}

	/**
	 * returns all labels
	 * 
	 * @return
	 */
	public List<GameWorldLabel> getLabels() {

		if (this.labels == null) {
			this.labels = getDataRetriever().getLabels(this.getResource());
		}
		logger.info("labels: {}", this.labels);

		return labels;
	}

	private List<GameWorldLabel> getFilterLabels(Language language, boolean flag) {
		if (this.labels == null) {
			this.labels = getDataRetriever().getLabels(this.getResource());
		}

		List<GameWorldLabel> filteredLabels = new LinkedList<>();
		for (GameWorldLabel label : this.labels) {
			if (label.isLanguage(language) == flag) {
				logger.debug("IF   label: {}, lang: {}", label, label.getLanguage());
				filteredLabels.add(label);
			} else {
				logger.debug("ELSE label: {}, lang: {}", label, label.getLanguage());
			}
		}
		
		logger.info("labels ({},{}): {}", language, flag, labels);
		return filteredLabels;
	}

	/**
	 * returns all labels
	 * 
	 * @return
	 */
	public List<GameWorldLabel> getLanguageLabels() {
		return this.getFilterLabels(DataRetriever.getLanguage(), true);
	}

	/**
	 * returns all labels
	 * 
	 * @return
	 */
	public List<GameWorldLabel> getOtherLabels() {
		return this.getFilterLabels(DataRetriever.getLanguage(), false);
	}

	/**
	 * returns all available properties
	 */
	public List<Entry<URI, Object>> getProperties() {
		// TODO: cover duplicate appearances of a property

		if (this.properties == null || this.properties.size() == 0) {
			this.properties = new LinkedList<>(getDataRetriever().getAllProperties(this.resource));
		}
		logger.info("properties: {}", this.properties);
		return this.properties;
	}

	/**
	 * get only the URI properties
	 * 
	 * @return
	 */
	public List<Entry<URI, Object>> getUriProperties() {
		List<Entry<URI, Object>> uriProperties = new LinkedList<>();
		for (Entry<URI, Object> entry : this.getProperties()) {
			if (! this.isValueALiteral(entry.getValue())) {
				logger.info("{} is a URI", entry.getValue());
				uriProperties.add(entry);
			} else {
				logger.info("{} is no URI", entry.getValue());
			}
		}

		return uriProperties;
	}

	/**
	 * get only the literal properties
	 * 
	 * @return
	 */
	public List<Entry<URI, Object>> getLiteralProperties() {
		List<Entry<URI, Object>> literalProperties = new LinkedList<>();
		for (Entry<URI, Object> entry : this.getProperties()) {
			if (this.isValueALiteral(entry.getValue())) {
				logger.info("{} is no URI", entry.getValue());
				literalProperties.add(entry);
			} else {
				logger.info("{} is a URI", entry.getValue());
			}
		}

		return literalProperties;
	}

	private boolean isValueALiteral(Object object) {

		try {
			new Float(object.toString());
			logger.info("isValueALiteral: {} -> Float",object);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			new URI(object.toString());
		} catch (Exception e) {
			logger.info("isValueALiteral: {} -> URI, {}",object,e.getMessage());			
			return true;
		}

		return false;
	}

	/**
	 * returns all connections
	 * 
	 * @return
	 */
	public List<GameWorldConnection> getConnections() {

		if (this.connections == null) {
			this.connections = getDataRetriever().getConnections(this.getResource());
			Collections.sort(this.connections);
		}
		logger.info("{} connections of {}", this.connections.size(), this.getResource());

		return this.connections;
	}

	/**
	 * returns all available images
	 */
	public List<URI> getImages() {
		return getDataRetriever().getAllImages(this.resource);
	}

	/**
	 * get all GameWorld instances of this type
	 * 
	 * @return
	 */
	public Collection<GameWorldInstance> getInstances() {

		// if not used before, then initialize and cache
		try {
			if (this.instances.size() == 0) {
				List<URI> classInstances = getDataRetriever().getGameWorldClassInstances(this.getResource());
				for (URI classInstanceURI : classInstances) {
					this.instances.add(GameWorldInstance.getInstance(classInstanceURI));
				}
			}
		} catch (Exception e) {
			logger.warn("instances could not be computed for {}", this.getResource());
		}

		return this.instances;
	}

}
