package de.hsanhalt.inf.ldsw.data;

import java.net.URI;
import java.util.Base64;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameWorldConnection implements Comparable<GameWorldConnection> {

	private static final Logger logger = LoggerFactory.getLogger(GameWorldConnection.class);
	
	private final URI connection;
	private final List<GameWorldLabel> labels;
	private final URI target;
	
	public GameWorldConnection(URI connection, List<GameWorldLabel> labels, URI target){
		
		logger.info("new Connection: {} -{}-> {}",connection,labels.size(),target);
		
		this.connection = connection;
		this.labels = labels;
		this.target = target;
	}
	
	public URI getConnection(){
		return this.connection;
	}
	
	public List<GameWorldLabel> getLabels(){
		return this.labels;
	}
	
	
	public URI getTarget(){
		return this.target;
	}

	
	public static String encodedURI(byte[] uri){
		String plainUri = new String(uri);
		if( plainUri.startsWith("http://www.hs-anhalt.de/gameworld/") ){
			return "/entity/"+Base64.getEncoder().encodeToString(uri)+"/";
		} else {
			return plainUri;
		}
		
	}
	
	public static String encodedURI(String uri){
		return GameWorldConnection.encodedURI(uri.getBytes());
	}
	
	public static String encodedURI(URI uri){
		return GameWorldConnection.encodedURI(uri.toString().getBytes());
	}	
	
	
	public String getTargetEncoded(){
		return GameWorldConnection.encodedURI(this.getTarget());
	}

	@Override
	public int compareTo(GameWorldConnection o) {
		if( o.getConnection().toString().compareTo(this.getConnection().toString()) > 0){
			System.out.println(o.getConnection().toString()+" > "+this.getConnection().toString()+" > 0");
			return 1;
		} else {
			System.out.println(o.getConnection().toString()+" > "+this.getConnection().toString()+" <= 0");
			return -1;
		}
	}
}
