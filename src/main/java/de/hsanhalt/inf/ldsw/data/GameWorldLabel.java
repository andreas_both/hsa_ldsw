package de.hsanhalt.inf.ldsw.data;

import de.hsanhalt.inf.ldsw.DataRetriever.Language;

public class GameWorldLabel {

	private final String myString;
	private final String labelPredicate;
	private final String myLanguage;
	
	public GameWorldLabel(String labelPredicate, String myString, String myLanguage){
		this.labelPredicate = labelPredicate;
		this.myLanguage = myLanguage;
		this.myString = myString;
	}
	

	/*
	 * returns the text of this label
	 *
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.myString;
	}
	
	public String getLanguage(){
		return this.myLanguage;		
	}
	
	public String getPredicate(){
		return this.labelPredicate;		
	}
	
	public String getPredicateAsClassString(){
		return this.labelPredicate.replace(":", "_");
	}
	
	public Boolean isLanguage(String lang){
		if( this.myLanguage.compareTo(lang) == 0){
			return true;
		} else {
			return false;
		}
	}
	
	public Boolean isLanguage(Language lang){
		switch (lang) {
		case DE:
			return this.isLanguage("de");
		case EN:
			return this.isLanguage("en");
		default:
			return null;
		}
	}
	
}
